﻿#include <iostream>
#include <string>

using std::string;
using std::cin;
using std::cout;
using std::endl;

class Player
{
    private:
        
        string  name;
        int     score;

    public:
    
        Player ()                            { name = "Безымянный"; score = 0;      }
        Player ( string _name , int _score ) { name = _name;        score = _score; }

        void    SetName ( string _name )     { name = _name;   }
        string  GetName ()                   { return name;    }
    
        void    SetScore ( int _score )      { score = _score; }
        int     GetScore ()                  { return score;   }
};

void Sort ( Player* ptrPlayerArray , int arrayLength )
{
    for ( int i = 0 ; i < arrayLength ; i++ )
    {
        if ( ( ptrPlayerArray + i )->GetScore() < ( ptrPlayerArray + i + 1 )->GetScore() )
        {
            Player tempPlayer = *(ptrPlayerArray + i);
            *(ptrPlayerArray + i) = *(ptrPlayerArray + i + 1);
            *(ptrPlayerArray + i + 1) = tempPlayer;
            Sort(ptrPlayerArray, arrayLength);
        }
    }
}

int main ()
{
    int playersCount;

    cout << "How many players do you want to create?" << endl;
    cin >> playersCount;

    Player* ptrPlayersArray = new Player[playersCount];

    for ( int i = 0 ; i < playersCount ; i++ )
    {
        string initName;
        int initScore;

        cout << "Enter the player's name #" << ( i + 1 ) << endl;

        cin >> initName;
        (ptrPlayersArray+i)->SetName( initName );

        cout << "Enter the player's score #" << (i + 1) << endl;
        cin >> initScore;
        (ptrPlayersArray + i)->SetScore( initScore );
    }

    Sort(ptrPlayersArray, playersCount);

    for ( int i = 0 ; i < playersCount ; i++ )
    {
       cout << (ptrPlayersArray + i)->GetName() << "\t" << (ptrPlayersArray + i)->GetScore() << endl;
    }

    delete[] ptrPlayersArray;

    return 0;
}

